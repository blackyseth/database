import java.sql.*;
import java.util.ArrayList;

public class Connector {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost/t_sys_enums";
    private static final String USER = "userek";
    private static final String PASS = "haselko";
    private Connection conn = null;


    public void Connect(){
        try {
            Class.forName(JDBC_DRIVER);
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void fullRecords(){
        System.out.println("Creating prepared statement...");
        PreparedStatement pstmt = null;
        String enumName = "";
        try {
            pstmt = conn.prepareStatement("INSERT INTO t_sys_enums VALUES(?,?,?,?,?)");
            for (int i = 1; i <= 100; i++) {
                    pstmt.setNull(1, 0);
                    pstmt.setInt(2, i);
                    pstmt.setString(3, "code test" + i);
                    pstmt.setString(4, "value test" + i);
                switch (i%10){
                    case 0:{enumName = "type1";break;}
                    case 1:{enumName = "sometype";break;}
                    case 2:{enumName = "newtype";break;}
                    case 3:{enumName = "idea";break;}
                    case 4:{enumName = "of";break;}
                    case 5:{enumName = "getting";break;}
                    case 6:{enumName = "thisgs";break;}
                    case 7:{enumName = "done";break;}
                    case 8:{enumName = "so";break;}
                    case 9:{enumName = "ijust";break;}
                }
                pstmt.setString(5, enumName);
                pstmt.executeUpdate();
            }
            pstmt.close();
            conn.close();
            System.out.println("Statement done :)");
        } catch (SQLException e) {e.printStackTrace();}
    }

    public void clearRecords(){
        System.out.println("Clearing your Table");
        PreparedStatement pstmt = null;
        String sql = "TRUNCATE TABLE t_sys_enums";
        String sql2 = "ALTER TABLE t_sys_enums AUTO_INCREMENT = 1";
        try{
            pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();
            pstmt = conn.prepareStatement(sql2);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
        }catch (SQLException e){e.printStackTrace();}
    }

    public ArrayList<SystemEnumeration> getRecords(String nameOfEnumeration){
        //Connecting to database was showed 10 times, and it annoyed me :D
        Connect();
        PreparedStatement pstmt = null;
        ArrayList<SystemEnumeration> records = new ArrayList<SystemEnumeration>();
        try{
            ResultSet resultSet;
            pstmt = conn.prepareStatement("SELECT id,enumId,code,velua FROM t_sys_enums WHERE t_sys_enums.enumerationName = ?");
            pstmt.setString(1, nameOfEnumeration);
            resultSet = pstmt.executeQuery();
            while(resultSet.next()){
                records.add(new SystemEnumeration(resultSet.getInt(1),
                                                    resultSet.getInt(2),
                                                    resultSet.getString(3),
                                                    resultSet.getString(4)));
            }
            pstmt.close();
            conn.close();
        }catch (SQLException e){e.printStackTrace();}
        return records;
    }

    public ArrayList<String> getEnumerationNames(){
        Connect();
        ArrayList<String> names = new ArrayList<String>();
        try{
            String sql = "SELECT DISTINCT enumerationName FROM t_sys_enums";
            ResultSet rs;

            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sql);
            while(rs.next())
            {
                int i=1;
                names.add(rs.getNString(i));
                i++;
            }
            stmt.close();
            conn.close();
        }catch (SQLException e){e.printStackTrace();}

        return names;
    }
}
