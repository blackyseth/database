
public class EnumerationName {

    private String name;

    public EnumerationName(String name){
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
