
public class SystemEnumeration {
    private int id;
    private int enumId;
    private String code;
    private String value;

    public SystemEnumeration(int id, int enumId, String code, String value) {
        this.id = id;
        this.enumId = enumId;
        this.code = code;
        this.value = value;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEnumId() {
        return enumId;
    }

    public void setEnumId(int enumId) {
        this.enumId = enumId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId(){
        return this.id;
    }
}
