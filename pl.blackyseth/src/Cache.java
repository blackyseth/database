import java.util.*;

public class Cache{

    private Set<EnumerationName> enumerationNameSet = new HashSet<EnumerationName>();
    private  Map<EnumerationName,List<SystemEnumeration>> items = new HashMap<EnumerationName,List<SystemEnumeration>>();
    private static Cache cache;
    private static Object token = new Object();

    private Cache(){
        getEnumerationNames();
        createMap();
    }

    public static Cache getInstance(){
        if(cache == null){
            synchronized(token)
            {
                if(cache==null)
                    cache = new Cache();
                System.out.println("\n\n New cache created \n\n");
                System.out.println(cache);
            }
        }
        return cache;
    }

    public static void resetCache(){
        if(cache != null){
            synchronized (token){
                if (cache!=null){
                    cache = new Cache();
                    System.out.println("\n\n Cache reset !!! \n\n");
                    System.out.println(cache);
                }
            }
        }else {
            getInstance();
        }
    }

    public void getEnumerationNames(){
        System.out.println("Getting EnumerationNames from Table");
        Connector connector = new Connector();
        ArrayList<String> names = connector.getEnumerationNames();
        for (String s : names) {
            enumerationNameSet.add(new EnumerationName(s));
        }
    }

    public void createMap(){
        System.out.println("Getting records from Table");
        Connector connector = new Connector();
        for (Iterator<EnumerationName> iterator = enumerationNameSet.iterator(); iterator.hasNext(); ) {
            Thread thread = new Thread();
            thread.start();
            System.out.println(thread.getName());
            EnumerationName next = iterator.next();
            items.put(next,connector.getRecords(next.getName()));
            System.out.println(next.getName());
        }
    }

    public void checkMap(){
        for(Map.Entry<EnumerationName,List<SystemEnumeration>> entry : items.entrySet()){
            Thread thread = new Thread();
            thread.start();
            System.out.println(thread.getName());
            System.out.println(entry.getKey().getName());
            for (int i = 0; i <entry.getValue().size(); i++) {

                System.out.println(
                        entry.getValue().get(i).getId()+" "+
                        entry.getValue().get(i).getEnumId()+" "+
                        entry.getValue().get(i).getCode()+" "+
                        entry.getValue().get(i).getValue()
                );
            }
        }

    }

    public void clearCache(){
        enumerationNameSet.clear();
        items.clear();
        try {
            this.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
