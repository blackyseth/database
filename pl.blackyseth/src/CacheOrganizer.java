
public class CacheOrganizer{
    Cache cache;
    public Cache getCache() {
        cache = Cache.getInstance();
        return cache;
    }

    public void reset(){
        if(cache!=null){
            Cache.resetCache();
        }else{
            getCache();
        }
    }

    public void getInsides(){
        if(cache!=null){
            cache.checkMap();
        }else{
            getCache();
            getInsides();
        }
    }

    public void clearCache(){
        if(cache!=null){
            cache.clearCache();
            cache=null;
            System.out.println("Cache deleted :)");
        }else{
            System.out.println("There is nothing to clear ;)");
        }
    }
}
